"""Write a program that prints the numbers from 1 to 100.
But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
For numbers which are multiples of both three and five print “FizzBuzz”.

"""
def recursion_fizz_buzz(number):
    if number == 100:
        print("Buzz 100")
    else:
        if number % 3 == 0 and number % 5 == 0:
            print("FizzBuzz", number)
        elif number % 3 == 0:
            print("Fizz", number)
        elif number % 5 == 0:
            print("Buzz", number)
        else:
            print(number)
        recursion_fizz_buzz(number + 1)


if __name__ == '__main__':
    recursion_fizz_buzz(1)
