""" A palindrome is a word, phrase, number or sequence of words that reads the same backwards as forwards.
Punctuation and spaces between the words or lettering is allowed.

YOU CAN'T USE BUILD IN REVERSE METHOD

Examples:

racecar
Dad
was it a cat I saw

Create a method which will check if given string is palindrome.
If it is it should print "This string is palindrome" if not print "This string is not palindrome"
"""
def recursion_palindrome(word, i, j):
    if i >= j:
        return True
    elif word[i].lower() != word[j].lower():
        return False
    else:
        return recursion_palindrome(word, i + 1, j - 1)


if __name__ == '__main__':
    word = str( input("Please enter a string: "))
    if recursion_palindrome(word, 0, len(word) - 1):
        print('This string is a palindrome')
    else:
        print('This string is not a palindrome')




