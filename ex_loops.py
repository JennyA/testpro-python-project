"""
You need to create a program, which will accept a string and check if total count on characters
 within the string is even it should return:
"String is even"
If not it should return:
"String is not even"
"""
def characters_count(word):
    if len(word) % 2 == 0:
        return "String is even"
    else:
        return "String is not even"


if __name__ == '__main__':
    input_value = str(input("Please enter a word :"))
    print(characters_count(input_value))
