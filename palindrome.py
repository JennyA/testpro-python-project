""" A palindrome is a word, phrase, number or sequence of words that reads the same backwards as forwards.
Punctuation and spaces between the words or lettering is allowed.

YOU CAN'T USE BUILD IN REVERSE METHOD

Examples:

racecar
Dad
was it a cat I saw

Create a method which will check if given string is palindrome.
If it is it should print "This string is palindrome" if not print "This string is not palindrome"
"""


def is_palindrome(word):
    i = 0
    j = len(word) - 1
    while i < j:
        if word[i].lower() != word[j].lower():
            return 'This string is not palindrome'
        i += 1
        j -= 1
    return 'This string is palindrome'


if __name__ == '__main__':
    input_string = str( input("Please enter a string: "))
    print(is_palindrome(input_string))
